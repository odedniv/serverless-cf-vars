const step0 = jest.fn((a) => {
  return {resources: Object.assign({}, a, {resAdded0: 'yes'}), vars: {}}
})
const step1 = jest.fn((a) => {
  return Object.assign({}, a, {resAdded1: 'yes'})
})
const step2 = jest.fn((a) => {
  return Object.assign({}, a, {resAdded2: 'yes'})
})
jest.setMock('./lib/replaceChildNodes', {step0, step1, step2})
const serverlessCfVars = require('./index')

describe('serverless-cf-vars', () => {
  it('Runs step0 on resources and outputs', () => {
    step0.mockClear()
    const serverless = {
      service: {
        provider: {
          compiledCloudFormationTemplate: {
            Resources: {
              resourceA: 'name a'
            },
            Outputs: {
              aOut: 'output value'
            },
          }
        }
      }
    }
    const sut = new serverlessCfVars(serverless, {})
    sut.hooks['package:setupProviderConfiguration']()
    expect(step0.mock.calls.length).toBe(2)
    expect(step0.mock.calls[0][0]).toEqual({ resourceA: 'name a' })
    expect(step0.mock.calls[1][0]).toEqual({ aOut: 'output value' })
    expect(serverless.service.provider.compiledCloudFormationTemplate.Resources.resAdded0).toBe('yes')
    expect(serverless.service.provider.compiledCloudFormationTemplate.Outputs.resAdded0).toBe('yes')
  })
  it('Runs step1 and step2 on resources and outputs', () => {
    step1.mockClear()
    const serverless = {
      service: {
        provider: {
          compiledCloudFormationTemplate: {
            Resources: {
              resourceA: 'name a'
            },
            Outputs: {
              aOut: 'output value'
            },
          }
        }
      }
    }
    const sut = new serverlessCfVars(serverless, {})
    sut.hooks['aws:package:finalize:mergeCustomProviderResources']()
    expect(step1.mock.calls.length).toBe(2)
    expect(step1.mock.calls[0][0]).toEqual({ resourceA: 'name a' })
    expect(step1.mock.calls[1][0]).toEqual({ aOut: 'output value' })
    expect(step2.mock.calls.length).toBe(2)
    expect(step2.mock.calls[0][0]).toEqual({ resourceA: 'name a', resAdded1: 'yes' })
    expect(step2.mock.calls[1][0]).toEqual({ aOut: 'output value', resAdded1: 'yes' })
    expect(serverless.service.provider.compiledCloudFormationTemplate.Resources.resAdded2).toBe('yes')
    expect(serverless.service.provider.compiledCloudFormationTemplate.Outputs.resAdded2).toBe('yes')
  })
})
